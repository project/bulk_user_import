
-- SUMMARY --

The user import module allow us to import users in bulk using csv import tool. 
Once this feature is enabled "Import Bulk Users" tab will be displayed and "Add multiple members" tab will not visible in class.

Sample file called "sample-file.csv" is also included in module directory. you can follow that format to import bulk user in class.

For a full description of the module, visit the project page:
  http://drupal.org/project/bulk_user_import

To submit bug reports and feature suggestions, or to track changes:
  http://drupal.org/project/issues/bulk_user_import


-- REQUIREMENTS --

None.


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/895232 for further information.


-- CONFIGURATION --

* Configure import bulk feature by going this url from admin configuration, admin >> config >> bulk_user_import >> configuration
  Check on "Enable Bulk User Import Tab" and save configuration


-- CONTACT --

Current maintainers:
* Vishal D. Sheladiya (vishalsheladiya) - http://drupal.org/user/2915771



